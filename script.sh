#!/bin/bash

docker rm -f zap
CONTAINER_ID=$(docker run -d --name zap -u zap -p 8090:8080 -i devsecopsacademy/zap2docker-stable zap.sh -daemon -host 0.0.0.0 -config api.disablekey=true -config api.addrs.addr.name=.* -config api.addrs.addr.regex=true)
sleep 10
docker cp TheContext.context $CONTAINER_ID:/home/zap
docker cp TheContext.context $CONTAINER_ID:/home/zap
docker exec $CONTAINER_ID zap-cli context import /home/zap/TheContext.context
docker exec $CONTAINER_ID zap-cli open-url  https://cift0bsflblkiilbx59f.devsecops-academy.com/bodgeit/
docker exec $CONTAINER_ID zap-cli spider --context-name TheContext --user-name Username https://cift0bsflblkiilbx59f.devsecops-academy.com/bodgeit/

echo "Starting quick scan" 

MY_COMMAND_OUTPUT=$(docker exec $CONTAINER_ID zap-cli quick-scan --recursive --spider -c TheContext -u Username https://cift0bsflblkiilbx59f.devsecops-academy.com/bodgeit/  )
echo "$MY_COMMAND_OUTPUT"

#THIS SECTION CHECKS IF THERE ARE VULNERABILITIES. IF YES, A REPORT IS GENERATED AND THE PIPELINE FAILS.
if echo "$MY_COMMAND_OUTPUT" | grep -q 'Alert'; then
    docker exec $CONTAINER_ID zap-cli report -o report.html -f html
    docker cp $CONTAINER_ID:/zap/report.html .
     exit 1

#IF THERE ARE NO VULNERABOLITIES, ONLY A REPORT IS GENERATED AND THE PIPELINE PASSES.
else
    docker exec $CONTAINER_ID zap-cli report -o report.html -f html
    docker cp $CONTAINER_ID:/zap/report.html .
     exit 0
 fi
